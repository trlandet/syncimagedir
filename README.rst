Synchronize image directories
=============================

This graphical file sync tool is made for two very specific use cases:

1) When started with two directories as command line arguments:

   Synchronize files from one image directory to another recursively (backup/update backup)

   * Copy over files that are present in dir1 and missing in dir2
   * Delete files that are present in dir2 and missing in dir1
     **except** if the extra file is a RAW image file and the
     corresponding JPEG is present in dir1

2) When started with two directories as command line arguments:

   Delete RAW image files from the directory if there is no matching JPEG file

You are always able to select which files are to be copied and deleted in the graphical user interface before anything happens.

The tool is meant for the following work flow:

1) Copy all images from camera to computer (JPEG + RAW)
2) Delete JPEG images that you do not want to keep
3) Use this tool to delete the corresponding RAW image files
4) Copy over the images to a backup location
5) Optional: delete RAW files from computer to save space
6) Sync any edits that were made after backup without worrying about deleting the RAW files in the backup location even though they are no longer present in the original location (as long as you have kept the corresponding JPEG image file)

This program comes with no support and may have bugs that delete all your files.

Copyright and license
---------------------

Written by Tormod Landet in Python with wxPython 4. Licensed under the Apache 2.0 license.

Releases
--------

* Version 1.0 - 2017-XX-YY Still not released
