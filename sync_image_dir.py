import sys, os
import wx
import wx.dataview as dv

# Use the built-in version of scandir/walk if possible, otherwise
# use the scandir module version
try:
    from os import scandir
except ImportError:
    from scandir import scandir
    

def scantree(dir_path, log=0):
    """
    Recursive scandir
    Yields parent path along with DirEntry
    """
    for entry in scandir(dir_path):
        if entry.name.startswith('.'):
            continue
        
        if log > 0 and entry.is_dir():
            print 'Scanning', entry.path
        
        yield dir_path, entry
        
        if entry.is_dir():
            for c_parent, child in scantree(entry.path, log=log-1):
                yield c_parent, child


###############################################################################
# Data model

class Directory(object):
    is_dir = True
    
    def __init__(self, parent, path, name):
        self.parent = parent
        self.children = []
        self.path = path
        self.name = name
        self.size = 0
        self.status = ''
        self.copy = False
        self.delete = False
    
    @staticmethod
    def scan(path):
        assert os.path.isdir(path)
        root = Directory(parent=None, path=path, name='ROOT')
        dirs = {path: root}
        
        # Scan through the file system
        for dir_path, entry in scantree(path, 1):
            parent = dirs[dir_path]
            
            if entry.is_dir():
                o = Directory(parent, entry.path, entry.name)
                parent.children.append(o)
                dirs[entry.path] = o
            
            elif entry.is_file():
                sr = entry.stat()
                o = File(parent, entry.path, entry.name, sr.st_size)
                parent.children.append(o)
            
            else:
                raise NotImplementedError('Entry type (symlink?) not implemented: %s' % entry.path)
        
        # Sort all directory contents by dir/file and then name
        for d in root.iter_dirs():
            d.children.sort(key=lambda node: (0 if node.is_dir else 1, node.name))
        
        return root
    
    def is_root(self):
        return self.parent is None
    
    def iter_dirs(self, include_self=True):
        """
        An iterator over directories (recursive) 
        """
        if include_self:
            yield self
        # Recursively yield child directories
        for c in self.children:
            if isinstance(c, Directory):
                for c2 in c.iter_dirs():
                    yield c2 


class File(object):
    is_dir = False
    
    # Extension map for equivalent extensions (lower case)
    ext_map = {'jpeg': 'jpg'}
    raw_ext = ('raf',)
    
    def __init__(self, parent, path, name, size):
        self.parent = parent
        self.path = path
        self.name = name
        self.size = size
        self.status = ''
        self.delete = False
        self.copy = False


def mark_delete_raw_without_jpg(root_dir):
    """
    Find RAW image files without corresponding JPEG file
    We delete these as the culling of images is done with
    JPEGS and we do not want RAW files being left behind
    
    No actual deletion will be done, just marking of the
    directory objects
    """
    for d in root_dir.iter_dirs():
        files = {}
        ext_files = {}
        for c in d.children:
            base, ext = os.path.splitext(c.name)
            ext = ext[1:].lower()
            ext = File.ext_map.get(ext, ext)
            assert not (base, ext) in files
            files[base, ext] = c
            ext_files.setdefault(ext, set()).add(base)
        
        for raw_ext in File.raw_ext:
            for base in ext_files.get(raw_ext, []):
                if base not in ext_files.get('jpg', ()):
                    files[base, raw_ext].delete = True


def mark_copy_delete_synctree(root_dir1, root_dir2):
    """
    Mark two directories to prepare for one way sync
    from dir1 to dir2:
    
    Actions:
    
    * Mark files that are present in dir1 and missing in
      dir2 as copy in dir1
    
    * Mark files that are present in dir2 and missing in
      dir1 as delete in dir2
      -  Except the file is a RAW and the corresponding
         jpg is present in dir1
    
    No actual deletion or copying is done, just marking
    of the file and directory objects
    """

###############################################################################
# Wx data model

# This model provides these data columns:
#
#     0. Name:         string
#     1. Size:         int
#     2. Status:       string
#     3. Delete/Copy:  bool
#
class DirectoryContentsModel(dv.PyDataViewModel):
    def __init__(self, root_node, sync_dir_type='source'):
        super(DirectoryContentsModel, self).__init__()
        self.root_node = root_node
        self.sync_dir_type = sync_dir_type
        
        # The PyDataViewModel derives from both DataViewModel and from
        # DataViewItemObjectMapper, which has methods that help associate
        # data view items with Python objects. Normally a dictionary is used
        # so any Python object can be used as data nodes. If the data nodes
        # are weak-referencable then the objmapper can use a
        # WeakValueDictionary instead.
        self.UseWeakRefs(True)

    # Report how many columns this model provides data for.
    def GetColumnCount(self):
        return 4

    def GetChildren(self, parent, children):        
        if not parent.IsOk():
            node = self.root_node
        else:
            node = self.ItemToObject(parent)
        
        if isinstance(node, Directory):
            for child in node.children:
                children.append(self.ObjectToItem(child))
                assert child.parent is node
            return len(node.children)
        else:
            return 0

    def IsContainer(self, item):
        if not item.IsOk():
            return True # Root is a container
        node = self.ItemToObject(item)
        return isinstance(node, Directory)

    def GetParent(self, item):
        assert item.IsOk(), 'GetParent should never be called with the root node'
        node = self.ItemToObject(item)
        if node.parent.is_root():
            return dv.NullDataViewItem
        else:
            return self.ObjectToItem(node.parent)

    def GetValue(self, item, col):
        node = self.ItemToObject(item)

        if isinstance(node, (Directory, File)):
            if col == 0:
                return node.name
            elif col == 1:
                return '{:,}'.format(node.size)
            elif col == 2:
                return node.status
            elif col == 3:
                return node.delete if self.sync_dir_type == 'target' else node.copy
        else:
            raise RuntimeError("unknown node type")
    
    def GetColumnType(self, col):
        return 'string' if col != 3 else 'bool'

    def GetAttr(self, item, col, attr):
        node = self.ItemToObject(item)
        if isinstance(node, Directory):
            attr.SetColour('blue')
            attr.SetBold(True)
            return True
        return False

    def SetValue(self, value, item, col):
        if not item.IsOk():
            return False
        node = self.ItemToObject(item)
        if isinstance(node, (Directory, File)):
            if col == 3:
                if self.sync_dir_type == 'target':
                    node.delete = value
                else:
                    node.copy = value
        return True


###############################################################################
# GUI

class SyncTreePanel(wx.Panel):
    def __init__(self, parent, dir_contents=None, model=None, sync_dir_type='source'):
        wx.Panel.__init__(self, parent, -1)
        
        if sync_dir_type == 'target':
            title1 = 'Target files'
            title2 = 'Files to delete'
            T3 = 'Delete'
        else:
            assert sync_dir_type == 'source'
            title1 = 'Source files'
            title2 = 'Files to copy'
            T3 = 'Copy'
        
        # Create the notebook
        nb = wx.Notebook(self)

        # Create a dataview control
        dvc = dv.DataViewCtrl(nb,
                              style=wx.BORDER_THEME
                                    | dv.DV_ROW_LINES # nice alternating bg colors
                                    #| dv.DV_HORIZ_RULES
                                    | dv.DV_VERT_RULES
                                    | dv.DV_MULTIPLE)
        nb.AddPage(dvc, title1, select=True)
        
        # Create an instance of our model...
        if model is None:
            model = DirectoryContentsModel(dir_contents, sync_dir_type=sync_dir_type)
        
        # Tel the DVC to use the model
        dvc.AssociateModel(model)

        # Define the columns that we want in the view.  Notice the
        # parameter which tells the view which col in the data model to pull
        # values from for each view column.
        c0 = dvc.AppendTextColumn("Name", 0, width=200, mode=dv.DATAVIEW_CELL_INERT)
        c1 = dvc.AppendTextColumn('Size', 1, width=100, mode=dv.DATAVIEW_CELL_INERT)
        c2 = dvc.AppendTextColumn('Status', 2, width=125, mode=dv.DATAVIEW_CELL_INERT)
        c3 = dvc.AppendToggleColumn(T3, 3, width=25, mode=dv.DATAVIEW_CELL_ACTIVATABLE)
        
        c1.Alignment = wx.ALIGN_RIGHT
        c2.Alignment = wx.ALIGN_LEFT
        c3.Alignment = wx.ALIGN_CENTER
        
        # Create the list of files to Copy / Delete 
        p = wx.Panel(nb)
        nb.AddPage(p, title2, select=False)
        
        self.Sizer = wx.BoxSizer(wx.VERTICAL)
        self.Sizer.Add(nb, 1, wx.EXPAND)


def cleanup_raf_gui(pth1):
    root = Directory.scan(os.path.abspath(pth1))
    mark_delete_raw_without_jpg(root)
    
    # GUI
    app = wx.App()
    frm = wx.Frame(None, title="RemoveStrayRAF", size=(1000,800))
    SyncTreePanel(frm, root, sync_dir_type='target')
    frm.Show()
    app.MainLoop()


def sync_image_dir_gui(pth1, pth2):
    dir1 = Directory.scan(os.path.abspath(pth1))
    dir2 = Directory.scan(os.path.abspath(pth2))
    mark_copy_delete_synctree
    
    # GUI
    width, height = 1000, 800
    min_width, min_height = 600, 200
    app = wx.App()
    frm = wx.Frame(None, title="SyncImageDir", size=(width, height))
    frm.SetMinSize((min_width, min_height))
    sp = wx.SplitterWindow(frm)
    p1 = SyncTreePanel(sp, dir1, sync_dir_type='source')
    p2 = SyncTreePanel(sp, dir2, sync_dir_type='target')
    sp.SplitVertically(p1, p2)
    sp.SetMinimumPaneSize(min_width//2)
    sp.SetSize((width, height))
    sp.SetSashGravity(0.5)
    sp.SetSashPosition(width//2)
    frm.Show()
    app.MainLoop()


def main(argv):
    if len(argv) == 2:
        root_pth = argv[1]
        cleanup_raf_gui(root_pth)
    else:
        assert len(argv) == 3
        dir_from = argv[1]
        dir_to = argv[2]
        sync_image_dir_gui(dir_from, dir_to)


if __name__ == '__main__':
    main(sys.argv)
