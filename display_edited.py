from __future__ import print_function
import sys, os, subprocess, io
import wx
from wx.lib.scrolledpanel import ScrolledPanel
from  wx.lib.statbmp import GenStaticBitmap
from PIL import Image
import cPickle as pickle


SIDECAR_EXT = '.xmp'
RAW_EXTS = ('.raf') 


# Use the built-in version of scandir/walk if possible, otherwise
# use the scandir module version
try:
    from os import scandir
except ImportError:
    from scandir import scandir


def find_raw_with_sidecar(dir_path, log=0):
    """
    Recursive scandir for sidecar files
    """
    results = []
    
    files = []
    sidecars = []
    for entry in scandir(dir_path):
        if entry.name.startswith('.'):
            continue
        
        if log > 0 and entry.is_dir():
            print('Scanning', entry.path)
        
        if entry.is_dir():
            results.extend(find_raw_with_sidecar(entry.path, log=log-1))
        elif entry.is_file():
            files.append(entry)
            if entry.path.endswith(SIDECAR_EXT):
                sidecars.append(entry)
    
    files.sort(key=lambda f: entry.path)
    sidecars.sort(key=lambda f: entry.path)
    
    for entry in sidecars:
        results.append([])
        base = os.path.splitext(entry.path[:-len(SIDECAR_EXT)])[0]
        
        for f in files:
            if f.path.startswith(base) and f.path != entry.path:
                results[-1].append(f.path)

    return results


class ThumbnailCache(object):
    def __init__(self, path='~/.cache/RawFilesWithSidecar.thumbs'):
        self.cache_path = os.path.abspath(os.path.expanduser(path))
        if os.path.isfile(self.cache_path):
            with open(self.cache_path, 'rb') as f:
                self.cache = pickle.load(f)
        else:
            self.cache = {}
    
    def get_thumbnail_data(self, path, image_size):
        """
        Return a PIL/Pillow Image thumbnail
        """
        path = os.path.abspath(path)
        key = (path, image_size)
        if key in self.cache:
            return self.cache[key]
        
        # Open PIL image and create thumbnail
        background = Image.new('RGBA', image_size, (255, 255, 255, 0))
        imdata = load_image(path)
        if imdata:
            imdata = io.BytesIO(imdata)
            image = Image.open(imdata)
            image.thumbnail(image_size, Image.ANTIALIAS)
            background.paste(image, (int((image_size[0] - image.size[0]) / 2), 
                                     int((image_size[1] - image.size[1]) / 2)))
        
        self.cache[key] = background
        return background
    
    def save(self):
        with open(self.cache_path, 'wb') as f:
            pickle.dump(self.cache, f, protocol=pickle.HIGHEST_PROTOCOL)


def load_image(path):
    ext = os.path.splitext(path)[1]
    
    if ext.lower() in RAW_EXTS:
        cmd = ['exiftool', '-b', '-ThumbnailImage', path]
        jpg_data = subprocess.check_output(cmd)
        return jpg_data
    else:
        return open(path, 'rb').read()


class ImageDisplay(wx.Frame):
    def __init__(self, file_groups):
        width, height = 1000, 800
        min_width, min_height = 200, 200
        grid_cols = 3
        imsize = (300, 300)
        
        super(ImageDisplay, self).__init__(None, title="RawFilesWithSidecar", size=(width, height))
        self.SetMinSize((min_width, min_height))
        
        p = ScrolledPanel(self)
        grid = wx.GridSizer(grid_cols, vgap=5, hgap=5)
        
        cache = ThumbnailCache()
        for fg in file_groups:
            for im_path in fg:
                grid.Add(ImagePanel(p, cache, im_path, imsize))
        cache.save()
        
        p.SetSizer(grid)
        p.SetupScrolling()


class ImagePanel(wx.Panel):
    def __init__(self, parent, cache, im_path, image_size):
        super(ImagePanel, self).__init__(parent)
        self.SetBackgroundColour(wx.Colour('white'))
        
        # Create wx image from PIL Image
        thumbnail = cache.get_thumbnail_data(im_path, image_size)
        wx_image = wx.Image(*image_size)
        imdata_rgb = thumbnail.copy().convert('RGB').tobytes()
        wx_image.SetData(imdata_rgb)
        wx_image.SetAlpha(thumbnail.tobytes()[3::4])
        
        # Bitmap and name to display
        sb = GenStaticBitmap(self, wx.ID_ANY, bitmap=wx_image.ConvertToBitmap())
        st = wx.StaticText(self, label=os.path.basename(im_path))
        
        v = wx.BoxSizer(wx.VERTICAL)
        flags = wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL
        v.Add(sb, flag=flags)
        v.Add(st, flag=flags)
        
        self.im_path = im_path
        for ob in (self, sb, st):
            ob.Bind(wx.EVT_LEFT_DCLICK, self.OpenImage)
        self.SetSizer(v)
    
    def OpenImage(self, evt=None):
        print(self.im_path)
        subprocess.Popen(['xdg-open', self.im_path])
        

if __name__ == '__main__':
    root_dir = sys.argv[1]
    file_groups = find_raw_with_sidecar(root_dir, 1)
    
    app = wx.App()
    frm = ImageDisplay(file_groups)
    frm.Show()
    app.MainLoop()